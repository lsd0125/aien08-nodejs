const sql = require('mssql');

const config = {
    // user: 'sa',
    // password: 'P@ssw0rd',
    user: 'root',
    password: 'root',
    server: 'localhost', // You can use 'localhost\\instance' to connect to named instance
    database: 'TestDB',
}
 
const h = async () => {
    try {
        // make sure that any items are correctly URL encoded in the connection string
        await sql.connect(config)
        const result = await sql.query(`select * from address_book`)
        console.dir(result)
    } catch (err) {
        console.log(err);
    }
};
h();